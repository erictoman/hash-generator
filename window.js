var sha1 = require('sha1');
const fs = require("fs")
var path = require('path');
var $ = require('jquery')
var {
    dialog
} = require("electron").remote

$(() => {
    $("#joinHASH").click(function () {
        openFile(true)
    })
    $("#separeHASH").click(function () {
        openFile(false)
    })
    $("#back").click(()=>{
        $("#modal").removeClass("is-active")
    })
})

function openFile(opt) {
    dialog.showOpenDialog({
        properties: ['openFile']
    }, (filePaths) => {
        if (filePaths !== undefined) {
            fs.readFile(filePaths[0], 'utf8', (err, data) => {
                if (opt) {
                    saveWithHash(filePaths[0], data)
                } else {
                    openWithHash(data)
                }
            });
        } else {
            alert("No file choosen")
        }
    })
}

function saveFile(file, content) {
    dialog.showSaveDialog({
        defaultPath: path.basename(file) + "_hash" + path.extname(file)
    }, (filename) => {
        if (filename !== undefined) {
            try {
                fs.writeFile(filename, content, () => {
                    alert("File saved")
                })
            } catch (err) {
                alert("Error")
            }
        } else {
            alert("File name not set")
        }
    })
}

function saveWithHash(file, content) {
    hash = sha1(content)
    content = content + "\n~HASH~\n" + hash
    saveFile(file, content)
}

function openWithHash(content){
    var contents=content.split("\n~HASH~\n")
    var sha = sha1(contents[0])
    if(contents.length==2){
        if(sha===contents[1]){
            $("#type").text("😃")
        }else{
            $("#type").text("😞")
        }
        $("#modal").addClass("is-active")
        console.log(sha)
        console.log(contents[1])
    }else{
        alert("Hash not found")
    }
}